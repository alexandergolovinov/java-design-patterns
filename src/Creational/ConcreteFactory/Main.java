package Creational.ConcreteFactory;

/**
 * Factory Design Pattern defines an interface for creating an object, but let subclasses decide which class to instantiate.
 * The Factory method lets a class defer instantiation to subclasses
 */
public class Main {

    public static void main(String[] args) {

        ShapeFactory shapeFactory = new ShapeFactory();

        Shape rectangle = shapeFactory.getShape("rectangle");
        rectangle.draw();

        Shape circle = shapeFactory.getShape("circle");
        circle.draw();

        Shape square = shapeFactory.getShape("square");
        square.draw();



    }
}
