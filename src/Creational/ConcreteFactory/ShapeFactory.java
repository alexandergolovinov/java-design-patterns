package Creational.ConcreteFactory;


public class ShapeFactory {

    public Shape getShape(String shapeName) {
        if (shapeName.equalsIgnoreCase("rectangle")) {
            return new Rectangle();
        } else if (shapeName.equalsIgnoreCase("circle")) {
            return new Circle();
        } else if (shapeName.equalsIgnoreCase("square")) {
            return new Square();
        } else {
            return null;
        }
    }
}
