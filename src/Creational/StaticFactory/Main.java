package Creational.StaticFactory;

/**
 * A static factory method is a public static method on the object that returns a new instance of the object.
 * These type of methods share the same benefits as the traditional factory method design pattern.
 * This is especially useful for value objects that don't have a separate interface and implementation class.
 */
public class Main {

    public static void main(String[] args) {

        Shape rectangle = ShapeFactory.getShape("rectangle");
        rectangle.draw();

        Shape circle = ShapeFactory.getShape("circle");
        circle.draw();

        Shape square = ShapeFactory.getShape("square");
        square.draw();
    }
}
