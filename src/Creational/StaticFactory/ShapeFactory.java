package Creational.StaticFactory;

public class ShapeFactory {

    public static Shape getShape(String shape) {
        if (shape.equalsIgnoreCase("rectangle")) {
            return new Rectangle();
        }
        if (shape.equalsIgnoreCase("circle")) {
            return new Circle();
        }
        if (shape.equalsIgnoreCase("square")) {
            return new Square();
        }
        return null;
    }

}
