package Creational.Singleton;

public class LazySingleton {

    //private reference to the one and only one instance within application
    private static LazySingleton uniqueInstance = null;

    //an instance attribute.
    private int data = 0;

    //private constructor to prevent instantiation
    private LazySingleton() {}

    //access point to the instance
    public static LazySingleton getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new LazySingleton();
        }
        return uniqueInstance;
    }

    public int getData() {
        return this.data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
