package Creational.Singleton;

public class CheckedSingleton {

    //private reference to the one and only one instance within application
    private static CheckedSingleton uniqueInstance = null;

    //an instance attribute.
    private int data = 0;

    //private constructor to prevent instantiation
    private CheckedSingleton() {}

    /**
     * synchronized forces every thread to wait it's turn to access this method
     * @return
     */
    public static synchronized CheckedSingleton getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new CheckedSingleton();
        }
        return uniqueInstance;
    }

    public int getData() {
        return this.data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
