package Creational.Singleton;

/**
 * Singleton design pattern is used when you want to have only one instance of a given class.
 * It is a creational design pattern wherein we deal with the creation of objects.
 */
public class Main {
    public static void main(String[] args) {
        LazySingleton singleton = LazySingleton.getInstance();
        singleton.setData(5);
        System.out.println(singleton.getData()); //5

        //It is the same object.
        LazySingleton singleton1 = LazySingleton.getInstance();

        System.out.println(singleton1.getData()); //5
    }
}
