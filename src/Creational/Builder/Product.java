package Creational.Builder;

import java.util.LinkedList;

public class Product {

    private LinkedList<String> parts;

    public Product() {
        parts = new LinkedList<>();
    }

    public void add(final String part) {
        parts.addLast(part);
    }

    public void showParts() {
        parts.forEach(System.out::println);
    }
}
