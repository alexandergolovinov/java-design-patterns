package Creational.Builder;

/**
 * The builder pattern, as name implies, is an alternative way to construct complex objects.
 * This should be used only when you want to build different immutable objects using same object building process.
 */
public class Main {

    public static void main(String[] args) {
        Assembly assembly = new Assembly();

        BuilderInterface carBuilder = new Car();
        BuilderInterface motorcycleBuilder = new Motocycle();

        assembly.construct(carBuilder);
        Product car = carBuilder.getVehicle();
        car.showParts();

        assembly.construct(motorcycleBuilder);
        Product motorcycle = motorcycleBuilder.getVehicle();
        motorcycle.showParts();
    }

}
