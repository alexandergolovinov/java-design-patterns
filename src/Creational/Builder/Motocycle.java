package Creational.Builder;

public class Motocycle implements BuilderInterface {

    private Product product = new Product();

    @Override
    public void buildBody() {
        product.add("Adding motorcycle body");
    }

    @Override
    public void insertWheels() {
        product.add("Adding motorcycle wheels");
    }

    @Override
    public void addHeadlights() {
        product.add("Adding motorcycle lights");
    }

    @Override
    public Product getVehicle() {
        return product;
    }
}
