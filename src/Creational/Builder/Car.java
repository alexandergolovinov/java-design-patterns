package Creational.Builder;

public class Car implements BuilderInterface {

    private Product product = new Product();

    @Override
    public void buildBody() {
        product.add("Building car body");
    }

    @Override
    public void insertWheels() {
        product.add("Inserting car Wheels");

    }

    @Override
    public void addHeadlights() {
        product.add("Adding car headlights");

    }

    @Override
    public Product getVehicle() {
        return product;
    }
}
