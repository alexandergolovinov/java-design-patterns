package Creational.Builder;

/**
 * Interface contains methods that each concrete implementation has to implement
 */
public interface BuilderInterface {

    void buildBody();

    void insertWheels();

    void addHeadlights();

    Product getVehicle();

}
