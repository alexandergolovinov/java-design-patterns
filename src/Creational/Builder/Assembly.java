package Creational.Builder;

public class Assembly {

    private BuilderInterface myBuilder;

    public void construct(final BuilderInterface builder) {
        myBuilder = builder;
        myBuilder.buildBody();
        myBuilder.insertWheels();
        myBuilder.addHeadlights();
    }

}
