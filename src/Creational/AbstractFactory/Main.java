package Creational.AbstractFactory;

/**
 * Abstract Factory is a creational design pattern, which solves the problem of creating entire product families
 * without specifying their concrete classes. Abstract Factory defines an interface for creating all distinct products,
 * but leaves the actual product creation to concrete factory classes.
 */
public class Main {

    public static void main(String[] args) {

        Shape rectangle = new RectangleFactory().getShape();
        rectangle.draw();

        Shape circle = new CircleFactory().getShape();
        circle.draw();

        Shape square = new SquareFactory().getShape();
        square.draw();


    }

}
