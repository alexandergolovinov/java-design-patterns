package Creational.Prototype;

/**
 * A prototype is a template of any object before the actual object is constructed.
 * In java also, it holds the same meaning. Prototype design pattern is used in scenarios where application needs to create a number of instances of a class,
 * which has almost same state or differs very little
 */
public class Rectangle extends Shape {

    public Rectangle() {
        type = "Rectangle";
    }

    @Override
    public void draw() {
        System.out.println("Rectangle::draw() method");
    }
}
