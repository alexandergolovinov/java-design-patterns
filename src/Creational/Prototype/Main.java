package Creational.Prototype;

public class Main {

    public static void main(String[] args) {
        ShapeCache.loadCache();
        Shape clonedShape = ShapeCache.getShape("1");
        System.out.println(clonedShape.type);

        Shape clonedShape1 = ShapeCache.getShape("2");
        System.out.println(clonedShape1.type);

    }

}
