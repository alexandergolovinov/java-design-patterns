package Behavioral.Iterator;

/**
 * Iterator Design Pattern provides a way to access the elements of an aggregate (collection) object
 * sequentially without exposing its underlying representation
 */
public class Main {
    public static void main(String[] args) {
        NotificationCollection nc = new NotificationCollection();
        NotificationBar nb = new NotificationBar(nc);
        nb.printNotification();
    }
}
