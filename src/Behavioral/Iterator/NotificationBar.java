package Behavioral.Iterator;

public class NotificationBar {

    private NotificationCollection notifications;

    public NotificationBar(NotificationCollection notifications) {
        this.notifications = notifications;
    }

    public void printNotification() {
        Iterator iterator = notifications.createIterator();
        System.out.println("Notification bar");
        while (iterator.hasNext()) {
            Notification n = (Notification) iterator.next();
            System.out.println(n.getNotification());
        }
    }
}
