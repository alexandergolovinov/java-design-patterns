package Behavioral.Iterator;

public interface Collection {
    Iterator createIterator();
}

class NotificationCollection implements Collection {

    private static final int MAX_ITEMS = 6;
    private int numberOfItems = 0;
    private Notification[] notificationList;

    public NotificationCollection() {
        this.notificationList = new Notification[MAX_ITEMS];
        addItem("Notification 1");
        addItem("Notification 2");
        addItem("Notification 3");
    }

    public void addItem(final String str) {
        Notification notification = new Notification(str);

        if (numberOfItems >= MAX_ITEMS) {
            System.out.println("Max number reached");
        } else {
            notificationList[numberOfItems] = notification;
            numberOfItems++;
        }
    }

    @Override
    public Iterator createIterator() {
        return new NotificationIterator(notificationList);
    }
}
