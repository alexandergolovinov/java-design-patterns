package Behavioral.State;

/**
 * The State pattern allows an object to alter its behavior when its internal state changes.
 *
 * Object will appear to change its class. For each state there is different object.
 */
public class Main {

    public static void main(String[] args) {

        Off initialState = new Off();
        TV tv = new TV(initialState);

        tv.pressButton(); //Turns ON

        System.out.println("-----");

        tv.pressButton(); //Turns OFF

    }
}
