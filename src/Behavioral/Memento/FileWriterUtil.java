package Behavioral.Memento;

public class FileWriterUtil {
    private String filename;
    private StringBuilder content;

    public FileWriterUtil(String filename) {
        this.filename = filename;
        this.content = new StringBuilder();
    }

    @Override
    public String toString() {
        return this.content.toString();
    }

    public void write(final String str) {
        this.content.append(str);
    }

    //Create the memento
    public Memento save() {
        return new Memento(this.filename, this.content);
    }

    //Restore into its earlier state 'undo'
    public void undoToLastSave(final Object obj) {
        Memento memento = (Memento) obj;
        this.filename = memento.filename;
        this.content = memento.content;
    }

    private class Memento {
        private String filename;
        private StringBuilder content;

        public Memento(String filename, StringBuilder content) {
            this.filename = filename;
            this.content = new StringBuilder(content);
        }
    }
}
