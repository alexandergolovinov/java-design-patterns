package Behavioral.Memento;

/**
 * The Memento Design Pattern will capture and externalize an object's internal state so that the object
 * can be restored to this state later, without violating encapsulation
 *
 * The goal is to save the state of an object, so that in the future, we can go back to the specified state
 */
public class Main {
    public static void main(String[] args) {
        FileWriterCaretaker caretaker = new FileWriterCaretaker();

        FileWriterUtil fileWriter = new FileWriterUtil("/system/whatever/file.txt");
        fileWriter.write("First Set of Data: \n Myra \n Lucy \n");

        System.out.println("----SAVE DATA----");
        System.out.println(fileWriter);

        caretaker.save(fileWriter);

        fileWriter.write("Second Set of Data: \n Jason \n");
        System.out.println(fileWriter);

        System.out.println("----UNDO----");

        caretaker.undo(fileWriter);
        System.out.println(fileWriter);
    }
}
