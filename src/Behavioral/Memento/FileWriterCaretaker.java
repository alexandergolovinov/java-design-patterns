package Behavioral.Memento;

public class FileWriterCaretaker {

    private Object obj;

    public void save(FileWriterUtil fileWriter) {
        obj = fileWriter.save();
    }

    public void undo(FileWriterUtil fileWriter) {
        fileWriter.undoToLastSave(obj);
    }
}
