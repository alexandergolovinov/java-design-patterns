package Behavioral.Template;

public class ConcreteHouse extends HouseTemplate {
    @Override
    void buildWalls() {
        System.out.println("Building concrete walls");
    }

    @Override
    void buildPillars() {
        System.out.println("Building pillards with cement");
    }
}
