package Behavioral.Template;

abstract class HouseTemplate {
    //Template method, final so subclasses cannot override it
    public final void buildHouse() {
        buildFoundation();
        buildPillars();
        buildWalls();
        buildWindows();
        System.out.println("House is built");
    }

    private void buildWindows() {
        System.out.println("Building Glass Windows");
    }

    //Abstract methods with customizable implementation
    abstract void buildWalls();

    abstract void buildPillars();

    private void buildFoundation() {
        System.out.println("Building foundation with cement");
    }

}
