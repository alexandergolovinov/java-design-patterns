package Behavioral.Template;

public class WoodenHouse extends HouseTemplate {
    @Override
    void buildWalls() {
        System.out.println("Building wooden walls");
    }

    @Override
    void buildPillars() {
        System.out.println("Building wooden pillards");
    }
}

