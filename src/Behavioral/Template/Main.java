package Behavioral.Template;

/**
 * Defines the skeleton of an algorithm in an operation.
 * Helps us generalize a common process, at an abstract level.
 *
 * Abstract class defines the algorithm which cannot be overriden.
 */
public class Main {
    public static void main(String[] args) {
        HouseTemplate concreteHouse = new ConcreteHouse();
        HouseTemplate woodenHouse = new WoodenHouse();
        System.out.println("Concrete House building...");
        concreteHouse.buildHouse();

        System.out.println("-------\n");
        System.out.println("Wooden House building...");

        woodenHouse.buildHouse();
    }
}
