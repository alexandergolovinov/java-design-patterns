package Behavioral.Visitor;

public interface ShoppingCartVisitor {
    int visit(Book book);

    int visit(Fruit fruit);
}

class ShoppingCartVisitorImpl implements ShoppingCartVisitor {

    private int discount = 7;

    @Override
    public int visit(Book book) {
        if (book.getPrice() > 50) {
            int dicountValue = book.getPrice() - discount;
            System.out.println(book.getIsbnNumber() + " cost = " + dicountValue);
            return dicountValue;
        } else {
            System.out.println(book.getIsbnNumber() + " cost = " + book.getPrice());
            return book.getPrice();
        }
    }

    @Override
    public int visit(Fruit fruit) {
        int cost = fruit.getPricePerKg() * fruit.getWeight();
        System.out.println(fruit.getName() + " cost = " + cost);
        return cost;
    }
}