package Behavioral.Visitor;

/**
 * Visitor design pattern represents an operation to be performed on the elements of an object structure (collection, list, etc)
 *
 * Lets you define a new operation without changing the class of the elements in which it operates.
 */
public class Main {

    private static int calculatePrice(ItemElemenet[] itemElemenets) {
        ShoppingCartVisitor visitor = new ShoppingCartVisitorImpl();
        int sum = 0;
        for (ItemElemenet item : itemElemenets) {
            sum = sum + item.accept(visitor);
        }

        return sum;
    }

    public static void main(String[] args) {

        ItemElemenet[] items = new ItemElemenet[]{
                new Book(20, "101"),
                new Book(100, "102"),
                new Book(75, "103"),
                new Fruit(5, 5, "Banana"),
                new Fruit(6, 7, "Kiwi"),
                new Fruit(2, 14, "Apple")
        };

        int total = calculatePrice(items);
        System.out.println("Total cost: " + total);
    }
}
