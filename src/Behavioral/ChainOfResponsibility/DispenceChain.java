package Behavioral.ChainOfResponsibility;

public interface DispenceChain {
    void setNextChain(DispenceChain nextChain);

    void dispence(Currency currency);

}

class Dollar50Dispensor implements DispenceChain {

    private DispenceChain chain;

    @Override
    public void setNextChain(DispenceChain nextChain) {
        this.chain = nextChain;
    }

    @Override
    public void dispence(Currency currency) {
        if (currency.getAmount() >= 50) {
            int num = currency.getAmount() / 50;
            int remainder = currency.getAmount() % 50;
            System.out.println("Dispensing " + num + " 50$ note");
            if (remainder != 0) {
                this.chain.dispence(new Currency(remainder));
            }
        } else {
            this.chain.dispence(currency);
        }
    }
}

class Dollar20Dispenser implements DispenceChain {

    private DispenceChain chain;

    @Override
    public void setNextChain(DispenceChain nextChain) {
        this.chain = nextChain;
    }

    @Override
    public void dispence(Currency currency) {
        if (currency.getAmount() >= 20) {
            int num = currency.getAmount() / 20;
            int remainder = currency.getAmount() % 20;
            System.out.println("Dispensing " + num + " 20$ note");
            if (remainder != 0) {
                this.chain.dispence(new Currency(remainder));
            }
        } else {
            this.chain.dispence(currency);
        }
    }
}

class Dollar10Dispenser implements DispenceChain {

    private DispenceChain chain;

    @Override
    public void setNextChain(DispenceChain nextChain) {

    }

    @Override
    public void dispence(Currency currency) {
        if (currency.getAmount() >= 10) {
            int num = currency.getAmount() / 10;
            int remainder = currency.getAmount() % 10;
            System.out.println("Dispensing " + num + " 10$ note");
            if (remainder != 0) {
                this.chain.dispence(new Currency(remainder));
            }
        } else {
            this.chain.dispence(currency);
        }
    }
}
