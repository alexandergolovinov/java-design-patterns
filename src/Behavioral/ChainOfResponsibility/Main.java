package Behavioral.ChainOfResponsibility;

import java.util.Scanner;

/**
 * Chain Of responsibility is a source of command objects and a series of processing objects.
 */
public class Main {
    private DispenceChain c1;

    public Main() {

        //Initialize the chain
        this.c1 = new Dollar50Dispensor();
        DispenceChain c2 = new Dollar20Dispenser();
        DispenceChain c3 = new Dollar10Dispenser();

        //Set the chain of responsibility
        c1.setNextChain(c2);
        c2.setNextChain(c3);

    }

    public static void main(String[] args) {
        Main ATM = new Main();
        while (true) {
            int amount = 0;
            System.out.println("Enter amount to dispense: ");
            Scanner input = new Scanner(System.in);
            amount = input.nextInt();
            if (amount % 10 != 0) {
                System.out.println("Amount should be in multiple of 10s");
                return;
            }

            //Process the request
            ATM.c1.dispence(new Currency(amount));
        }
    }


}
