package Behavioral.Mediator;

/**
 *  Mediator Design Pattern is used to provide a centralized communication medium between different objects in a system.
 *
 * Defines an object that encapsulates how a set of objcts interact.
 * Takes responsibility of communication among a group of objects
 */
public class Main {
    public static void main(String[] args) {
        ChatMediator mediator = new ChatMediatorImpl();

        User user1 = new UserImpl(mediator, "Alexander");
        User user4 = new UserImpl(mediator, "Jason");
        User user2 = new UserImpl(mediator, "Jenifer");
        User user3 = new UserImpl(mediator, "Lucie");

        mediator.addUser(user1);
        mediator.addUser(user2);
        mediator.addUser(user3);
        mediator.addUser(user4);

        user1.send("Hi all");
    }
}
