package Behavioral.Observer;

public interface Observer {
    // Method to update the observer, used by the subject
    void update();

    //observer can query the subject to see if an update has occured
    void setSubject(Subject obj);
}

class MyTopicSubscriber implements Observer {

    private String name;
    private Behavioral.Observer.Subject topic;

    public MyTopicSubscriber(String name) {
        this.name = name;
    }

    @Override
    public void update() {
        String msg = (String) topic.getUpdate(this);
        if (msg == null) {
            System.out.println("No message");
        } else {
            System.out.println(name + " Consuming message: " + msg);
        }
    }

    @Override
    public void setSubject(Behavioral.Observer.Subject subject) {
        this.topic = subject;
    }
}
