package Behavioral.Observer;

import java.util.ArrayList;
import java.util.Observer;
import java.util.List;

public interface Subject {

    //Methods to register and unregister observers
    void register(Behavioral.Observer.Observer obj);

    void unregister(Behavioral.Observer.Observer obj);

    //Methods to notify observers of change
    void notifyObservers();

    public Object getUpdate(Behavioral.Observer.Observer obj);
}

class MyTopic implements Subject {
    private List<Behavioral.Observer.Observer> observers;
    private String message;
    private boolean changed;

    MyTopic() {
        this.observers = new ArrayList<>();
    }

    @Override
    public void register(Behavioral.Observer.Observer obj) {
        if (obj == null) {
            throw new NullPointerException("Null Observer");
        }

        if (!observers.contains(obj)) {
            observers.add(obj);
        }
    }

    @Override
    public void unregister(Behavioral.Observer.Observer obj) {
        observers.remove(obj);
    }

    @Override
    public void notifyObservers() {
        List<Behavioral.Observer.Observer> observersLoc = null;
        if (!changed) {
            return;
        }
        observersLoc = new ArrayList<>(this.observers);
        this.changed = false;

        for (Behavioral.Observer.Observer observer : observersLoc) {
            observer.update();
        }
    }

    @Override
    public Object getUpdate(Behavioral.Observer.Observer obj) {
        return this.message;
    }


    public void postMessage(String msg) {
        System.out.println("Message posted to topic: " + msg);
        this.message = msg;
        this.changed = true;
        notifyObservers();
    }
}
