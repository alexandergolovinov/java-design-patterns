package Behavioral.Strategy;

/**
 * Strategy Design Pattern is used when we have multiple algorithm for a specific task and client decides the actual implementation to be used at runtime.
 */
public class Main {

    public static void main(String[] args) {
        ShoppingCart cart = new ShoppingCart();
        Item item1 = new Item("101", 10);
        Item item2 = new Item("102", 17);

        cart.addItem(item1);
        cart.addItem(item2);

        //Choose Strategy

        //Pay by PayPal.
        cart.pay(new PayPalStrategy("alex@gmail.com", "#asd277/a#$%"));

        //Pay by CreditCard
        cart.pay(new CreditCardStrategy("alexander", "342", "779", "14102009"));
    }
}
