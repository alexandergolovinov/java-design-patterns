package Behavioral.Strategy;

import java.util.ArrayList;
import java.util.List;

class ShoppingCart {

    List<Item> items;

    public ShoppingCart() {
        this.items = new ArrayList<>();
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void removeItem(Item item) {
        items.remove(item);
    }

    public int calculateTotal() {
        return items.stream()
                .mapToInt(Item::getPrice).sum();
    }

    public void pay(PaymentStrategy paymentStrategy) {
        int amount  = calculateTotal();
        paymentStrategy.pay(amount);
    }

}
