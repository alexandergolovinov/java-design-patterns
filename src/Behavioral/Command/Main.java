package Behavioral.Command;

/**
 * Command Design Pattern encapsulates a request as an object (command)
 * Lets you parameterized clients with different requests
 */
public class Main {
    public static void main(String[] args) {
        //Creating the command

        OpenFileCommand openFileCommand = new OpenFileCommand(new UnixFileSystemReceiver());
        FileInvoker myFile = new FileInvoker(openFileCommand);
        myFile.execute();
    }
}
