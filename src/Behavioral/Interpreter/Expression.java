package Behavioral.Interpreter;

public interface Expression {
    String interpret(InterpreterContext ic);
}

class IntToBinaryExpressions implements Expression {
    private int i;

    public IntToBinaryExpressions(int i) {
        this.i = i;
    }

    @Override
    public String interpret(InterpreterContext ic) {
        return ic.getBinaryFormat(i);
    }
}

class IntToBinaryHex implements Expression {
    private int i;

    public IntToBinaryHex(int i) {
        this.i = i;
    }

    @Override
    public String interpret(InterpreterContext ic) {
        return ic.getHexadecimalFormat(i);
    }
}