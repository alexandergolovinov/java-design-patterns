package Behavioral.Interpreter;

/**
 * In short, the Interpreter Pattern defines the grammar of a particular language in an object-oriented way
 * which can be evaluated by the interpreter itself.
 */
public class Main {

    private static final String HEX = "Hexadecimal";
    private static final String BINARY = "Binary";
    public InterpreterContext ic;

    public Main(InterpreterContext ic) {
        this.ic = ic;
    }

    public String interpret(final String str) {
        Expression expression = null;

        if (str.toLowerCase().contains(HEX)) {
            expression = new IntToBinaryHex(Integer.parseInt(str.trim()));
        } else if (str.toLowerCase().contains(BINARY)) {
            expression = new IntToBinaryExpressions(Integer.parseInt(str.trim()));
        } else {
            return str;
        }

        return expression.interpret(ic);

    }
}
