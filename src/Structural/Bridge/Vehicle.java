package Structural.Bridge;

abstract class Vehicle {
    protected Workshop workshop1;
    protected Workshop workshop2;

    public Vehicle(Workshop w1, Workshop w2) {
        this.workshop1 = w1;
        this.workshop2 = w2;
    }

    abstract public void manufacture();
}

class Car extends Vehicle {

    public Car(Workshop w1, Workshop w2) {
        super(w1, w2);
    }

    @Override
    public void manufacture() {
        System.out.println("Car ");
        workshop1.work();
        workshop2.work();
    }
}

class Motorcycle extends Vehicle {

    public Motorcycle(Workshop w1, Workshop w2) {
        super(w1, w2);
    }

    @Override
    public void manufacture() {
        System.out.println("Motorcycle ");
        workshop1.work();
        workshop2.work();
    }
}
