package Structural.Bridge;

/**
 * The bridge design pattern is a structural pattern used to decouple an abstraction from its implementation so that the two can vary independently.
 */
public class Main {
    public static void main(String[] args) {
        Vehicle vehicle = new Car(new Produce(), new Asseble());
        vehicle.manufacture();

        Vehicle vehicle2 = new Motorcycle(new Produce(), new Asseble());
        vehicle2.manufacture();
    }
}
