package Structural.Bridge;


abstract class Workshop {
    abstract public void work();
}

class Produce extends Workshop {

    @Override
    public void work() {
        System.out.println("Produced");
    }
}

class Asseble extends Workshop {
    @Override
    public void work() {
        System.out.println("Assembled");
    }
}


