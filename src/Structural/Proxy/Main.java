package Structural.Proxy;

/**
 * Proxy Design Pattern. Proxy design pattern intent according to Gang of Four is:
 * Provide a surrogate or placeholder for another object to control access to it.
 * The definition itself is very clear and proxy design pattern is used when we want to provide controlled access of a functionality.
 */
public class Main {

    public static void main(String[] args) {
        Image image = new RealImage("someFile.jpg");
        image.display();
        System.out.println(" ");
        image.display();

    }

}
