package Structural.Proxy;

/**
 * Proxy Design Pattern. Proxy design pattern intent according to GoF is:
 * Provide a surrogate or placeholder for another object to control access to it.
 * The definition itself is very clear and proxy design pattern is used when we want to provide controlled access of a functionality.
 */
public class Proxy implements Image {

    private RealImage realImage;
    private String fileName;

    public Proxy(RealImage realImage, String fileName) {
        this.realImage = realImage;
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if (realImage == null) {
            realImage = new RealImage(fileName);
        }
        realImage.display();
    }
}
