package Structural.Adapter;

public interface Goose {

    void quack();
}

class CanadianGoose implements Goose {

    @Override
    public void quack() {
        System.out.println("Quack Quack!");
    }
}


