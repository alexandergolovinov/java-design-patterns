package Structural.Adapter;

/**
 * The interface of the type we are adapting to
 * In this case we adapt Goose to Cat.
 * Goose does not have purr method.
 */
public class GooseAdapter implements Goose{

    /**
     * The type adapt
     */
    Cat cat;

    public GooseAdapter(Cat cat) {
        this.cat = cat;
    }

    @Override
    public void quack() {
        cat.purr();
    }
}
