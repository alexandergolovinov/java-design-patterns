package Structural.Adapter;

public interface Cat {
    void purr();
}

class WildCat implements Cat {

    @Override
    public void purr() {
        System.out.println("Mrrr");
    }

}
