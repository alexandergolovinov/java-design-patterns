package Structural.Adapter;

/**
 * Adapter design pattern is one of the structural design pattern and its used so that two unrelated interfaces can work together.
 * The object that joins these unrelated interface is called an Adapter.
 *
 * Now we make goose to act like a cat using the Adapter.
 * We pass the instance of the cat into the adapter and through the Adapter call the methods of Cat.
 */
public class Main {

    public static void main(String[] args) {
        Cat cat = new WildCat();

        Goose gooseAdapter = new GooseAdapter(cat);

        gooseAdapter.quack();
    }

}
