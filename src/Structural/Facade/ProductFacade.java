package Structural.Facade;

public class ProductFacade {

    private Product product;

    public ProductFacade(Product product) {
        this.product = product;
    }

    public void movieInfo() {
        final String name = product.getName();
        final String genre = product.getGenre();
        System.out.println(name + ", " + genre);
    }
}
