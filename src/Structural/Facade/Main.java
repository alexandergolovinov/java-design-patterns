package Structural.Facade;

/**
 * Facade Pattern is doing the simplification of underlying logic.
 */
public class Main {
    public static void main(String[] args) {
        Product product = new Product();
        product.setName("Green Mile");
        product.setGenre("Family");

        ProductFacade facade = new ProductFacade(product);
        facade.movieInfo();
    }
}
