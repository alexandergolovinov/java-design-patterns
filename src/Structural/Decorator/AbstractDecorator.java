package Structural.Decorator;

abstract class AbstractDecorator extends Component {
    protected Component component;

    public void setComponent(Component component) {
        this.component = component;
    }

    public void doJob() {
        if (component != null) {
            component.doJob();
        }
    }
}

class ConcreteDecoratorExample1 extends AbstractDecorator {
    @Override
    public void doJob() {
        super.doJob();
        //Additional responsibilities on the component
        System.out.println("I am explicitly from ConcreteDecorator1");
    }
}


class ConcreteDecoratorExample2 extends AbstractDecorator {
    @Override
    public void doJob() {
        System.out.println("START Ex-2***");
        super.doJob();
        //Additional responsibilities on the component
        System.out.println("I am explicitly from ConcreteDecorator2");
    }
}