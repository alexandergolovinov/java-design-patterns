package Structural.Decorator;

abstract class Component {
    abstract void doJob();
}

class ConcreteComponent extends Component {

    @Override
    void doJob() {
        System.out.println("I am from Concrete Component.");
    }
}
