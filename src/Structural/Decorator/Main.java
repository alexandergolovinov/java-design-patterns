package Structural.Decorator;

/**
 * The decorator design pattern allows us to dynamically add functionality and behavior to an object
 * without affecting the behavior of other existing objects in the same class.
 */
public class Main {

    public static void main(String[] args) {
        ConcreteComponent cc = new ConcreteComponent();

        ConcreteDecoratorExample1 cd1 = new ConcreteDecoratorExample1();
        cd1.setComponent(cc);
        cd1.doJob();

    }

}
