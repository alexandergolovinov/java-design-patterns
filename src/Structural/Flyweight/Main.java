package Structural.Flyweight;

/**
 * The Flyweight Pattern reuses existing objects instead of creating new ones.
 */
public class Main {
    public static void main(String[] args) {

        RobotFactory robotFactory = new RobotFactory();
        RobotInterface robot;

        try {
            robot = robotFactory.getRobotFromFactory("robot-human");
            robot.print();

            for (int i = 0; i < 5; ++i) {
                robot = robotFactory.getRobotFromFactory("robot-human");
            }
            int numOfRobots = robotFactory.totalRobotsCreated();
            System.out.println(numOfRobots);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }
}
