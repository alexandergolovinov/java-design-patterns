package Structural.Flyweight;

public interface RobotInterface {
    void print();
}

class RobotMachine implements RobotInterface {
    @Override
    public void print() {
        System.out.println("Robot machine");
    }
}

class RobotHuman implements RobotInterface {
    @Override
    public void print() {
        System.out.println("Robot human");
    }
}


