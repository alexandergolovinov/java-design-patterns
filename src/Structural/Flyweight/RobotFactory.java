package Structural.Flyweight;

import java.util.HashMap;
import java.util.Map;

public class RobotFactory {

    Map<String, RobotInterface> robots = new HashMap<>();

    public int totalRobotsCreated() {
        return robots.size();
    }

    public RobotInterface getRobotFromFactory(final String robotCategory) throws Exception {
        RobotInterface myRobot = null;
        if (robots.containsKey(robotCategory)) {
            myRobot = robots.get(robotCategory);
        } else {
            switch (robotCategory) {
                case "robot-human":
                    myRobot = new RobotHuman();
                    robots.put("robot-human", myRobot);
                    break;
                case "robot-machine":
                    myRobot = new RobotMachine();
                    robots.put("robot-machine", myRobot);
                    break;
                default:
                    throw new Exception("No roobts found in factory");
            }
        }
        return myRobot;
    }
}
