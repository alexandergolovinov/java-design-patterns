package Structural.Composite;

import java.util.ArrayList;
import java.util.List;

public class Directory implements Employee {

    private List<Employee> employeeList = new ArrayList<>();

    @Override
    public void showEmployeeDetails() {
        for (Employee e : employeeList) {
            e.showEmployeeDetails();
        }
    }

    public void addEmployee(Employee e) {
        employeeList.add(e);
    }

    public void removeEmployee(Employee e) {
        employeeList.remove(e);
    }

}
