package Structural.Composite;

/**
 * Composite Design - composes objects into tree structure to represent part-whole hierarchies.
 * Composite lets client treat individual objects and compositions of objects uniformly
 */
public class Main {

    public static void main(String[] args) {
        Employee dev1 = new Developer("Alexander", "Pro", 101);
        Employee dev2 = new Developer("John", "Pro", 102);

        Directory devDirectory = new Directory();
        devDirectory.addEmployee(dev1);
        devDirectory.addEmployee(dev2);

        Employee man1 = new Manager("Jessica", "SEO Manager", 201);
        Employee man2 = new Manager("Ksenia", "Dev Manager", 202);

        Directory managerDirectory = new Directory();
        managerDirectory.addEmployee(man1);
        managerDirectory.addEmployee(man2);

        //Here we add composite objects into the same object

        Directory companyDirectory = new Directory();
        companyDirectory.addEmployee(devDirectory);
        companyDirectory.addEmployee(managerDirectory);

        companyDirectory.showEmployeeDetails();
    }

}
