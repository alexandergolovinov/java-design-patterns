package Structural.Composite;

public interface Employee {
    void showEmployeeDetails();
}

class Developer implements Employee {

    private String name;
    private String position;
    private int id;

    public Developer(String name, String position, int id) {
        this.name = name;
        this.position = position;
        this.id = id;
    }


    @Override
    public void showEmployeeDetails() {
        System.out.println(id + " " + name + " " + position);
    }
}

class Manager implements Employee {

    private String name;
    private String position;
    private int id;

    public Manager(String name, String position, int id) {
        this.name = name;
        this.position = position;
        this.id = id;
    }

    @Override
    public void showEmployeeDetails() {
        System.out.println(id + " " + name + " " + position);
    }
}
